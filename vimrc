""" TABS AS 4 SPACES AND LN
set tabstop=4
set shiftwidth=4
set expandtab
set relativenumber
set number

""" COLORS
syntax on
set t_Co=256

hi! Normal ctermbg=NONE   
colorscheme jellybeans

""" SEARCH
set incsearch
set hlsearch
hi Search cterm=NONE
hi Search ctermfg=0
hi Search ctermbg=110
""" #8fbfdc

" do not override termial bg NOT WORKING!!!
let g:jellybeans_overrides = {
\    'background': { 'ctermbg': 'black', '256ctermbg': 'blue' },
\}
if has('termguicolors') && &termguicolors
    let g:jellybeans_overrides['background']['guibg'] = 'none'
endif
" use italics
let g:jellybeans_use_term_italics = 1

"""

"" STATUS LINE 
function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set laststatus=2
set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=

"""

""" plug
call plug#begin('~/.vim/plugged')
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/tpope/vim-fugitive.git'
call plug#end()
