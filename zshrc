autoload -U colors && colors
cd
. /home/hubert/.profile

setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' stagedstr 'M'
zstyle ':vcs_info:*' unstagedstr 'U'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' actionformats '%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f'
zstyle ':vcs_info:*' formats \
    ' %F{9}%\.%s/%b%f' #%F{2}%c%F{3}%u%f'  #uncomment when you will know how it works
    #zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
    zstyle ':vcs_info:*' enable git svn cvs 
    +vi-git-untracked() {
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        [[ $(git ls-files --other --directory --exclude-standard | sed q | wc -l | tr -d ' ') == 1 ]] ; then
            hook_com[unstaged]+='%F{1}??%f'
    fi
}

precmd() { vcs_info }
PROMPT='%B%{$fg[white]%}[%{$fg[green]%}%n%{$fg[white]%}@%{$fg[green]%}%M %{$fg[blue]%}%~%{$fg[white]%}]%{$reset_color%}${vcs_info_msg_0_}%b$ '


# Report command running time if it is more than 3 seconds
REPORTTIME=3
# Keep a lot of history
HISTFILE=~/.cache/zsh/.zhistory
HISTSIZE=5000
SAVEHIST=5000

#turn off f*** bell
unsetopt BEEP

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.
#setopt correct_all
#ENABLE_CORRECTION="true"

bindkey -v
export KEYTIMEOUT=1
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[8~" end-of-line
bindkey "^[[7~" beginning-of-line
bindkey "^[[3~" delete-char
#bindkey -M vicmd "^[[3~" delete-char
#bindkey "^[3;5~" delete-char
#bindkey -M vicmd "^[[1;5D" backward-word 
#bindkey -M vicmd "^[[1;5C" forward-word 
#bindkey "^?" backward-delete-char
#bindkey "^[[8~" end-of-line
#bindkey "^[[7~" beginning-of-line


#vi mode
#bindkey -v
#export KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymaps-select {
if [[ ${KEYMAP} == vicmd ]] ||
    [[ $1 = 'block' ]]; then
    echo -ne '\e[2 q'
elif [[ ${KEYMAP} == main ]] ||
    [[ ${KEYMAP} == viins ]] ||
    [[ ${KEYMAP} = '' ]] ||
    [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
fi
}
#zle -N zle-keymaps-select

zle-line-init() {
zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
#echo -ne "\e[2 q"
}
zle -N zle-line-init

#edit cmd with vim
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

#auto cd
setopt auto_cd

#cp working directory
cpwd() { pwd > /tmp/tmp_pwd; }

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# colors in man pages
export LESS="--RAW-CONTROL-CHARS"
[[ -f "$HOME/.config/.LESS_TERMCAP" ]] && source "$HOME/.config/.LESS_TERMCAP"

#export LESS=-R
#export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
#export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
#export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
#export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
#export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
#export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
#export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

#syntax highlighting plugin
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

#do not 
ZSH_HIGHLIGHT_STYLES[path]=none
ZSH_HIGHLIGHT_STYLES[path_prefix]=none

#auto-sugestion plugin
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null

#diffrent color depending on the file extension
eval $(dircolors -b $HOME/.config/.dircolors)

if [[ $TERM == 'linux' ]] tZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=0,bold'
    export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=0,bold'
